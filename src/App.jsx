
import './App.css'

import Header from './components/Header'
import { BrowserRouter, Routes, Route, Outlet } from 'react-router-dom'
import HomePage from './components/HomePage'
import BoardDetail from './components/BoardDetail'

function App() {

  return (
    <>
        <Header />
        <Outlet/>
    </>

  )
}

export default App
