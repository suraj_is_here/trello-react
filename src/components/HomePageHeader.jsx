import { Box, Heading } from '@chakra-ui/react'
import React from 'react'

function HomePageHeader() {
    return (
        <Box
        display='flex'
        alignItems='center'
        paddingLeft='10'
            h = '10rem'
            w = '64rem'
            borderBottom='1px'
            borderBottomColor='silver'
        >
            <Heading>Suraj Kumar's workspace</Heading>
        </Box>
    )
}

export default HomePageHeader