import React, { useState, useEffect } from 'react'
import {
    Card, CardBody, Box, Button, Text, Flex, Spacer,useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
} from '@chakra-ui/react'
import { IoMdCheckboxOutline } from "react-icons/io";
import { FaTrash } from "react-icons/fa";
import axios from 'axios';
import CheckList from './CheckList';
import CreateCheckList from './CreateCheckList';
import {apiKey,apiToken} from '../Credential'

function FetchCard({ card, handleCardDelete }) {

    const { isOpen, onOpen, onClose } = useDisclosure()
    const [allCheckList, setAllCheckList] = useState([])
    const [isFormVisible,setIsFormVisible] = useState(false);
    const archiveCard = (e) => {
        console.log(card.id);
        e.stopPropagation();    
        axios
            .delete(
                `https://api.trello.com/1/cards/${card.id}?key=${apiKey}&token=${apiToken}`
            )
            .then(handleCardDelete(card.id));
    };

    useEffect(() => {
        axios
            .get(
                ` https://api.trello.com/1/cards/${card.id}/checklists?key=${apiKey}&token=${apiToken}`
            )
            .then((response) => setAllCheckList(response.data))
            .catch((error) => console.log(error));

    }, []);

    

    const addOnClickHandler = () => {
        isFormVisible?setIsFormVisible(false):setIsFormVisible(true)
    }
    const addNewCheckList = (newCheckList) => {
        setAllCheckList([...allCheckList,newCheckList])
    }

    const deleteChecklistHandler = (id) =>{
        console.log("Delete checklist called");
        console.log(id);
        const newCheckList = allCheckList.filter(checkList => checkList.id !== id)
        console.log(newCheckList);
        setAllCheckList(newCheckList);
    }

    return (
        <>
            <Card my='2' size='sm' shadow='lg' onClick={onOpen} >
                <CardBody  >
                    <Flex>
                        <Text>{card.name}</Text>
                        <Spacer />
                        <FaTrash onClick={(e) => archiveCard(e)} />
                    </Flex>

                </CardBody>
            </Card>

            

            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent minW='50rem'>
                    <ModalHeader>{card.name}</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Flex>
                            <Box minW = '31rem' >
                                {
                                
                                allCheckList.map((checklist) => {
                                    return (<CheckList key = {checklist.id} checklist={checklist} deleteChecklistHandler = {deleteChecklistHandler} cardId = {card.id} />)
                                })}

                            </Box>
                            
                            <Box>
                                <Button colorScheme='blue' mr={3} onClick={addOnClickHandler} gap='1' >
                                    <span><IoMdCheckboxOutline /></span>
                                    <span>Checklist</span>
                                </Button>
                                {isFormVisible?<CreateCheckList addNewCheckList = {addNewCheckList} addOnClickHandler ={addOnClickHandler} id = {card.id} />:''}
                                
                            </Box>

                        </Flex>
                    </ModalBody>
                </ModalContent>
            </Modal>
        </>

    )
}

export default FetchCard