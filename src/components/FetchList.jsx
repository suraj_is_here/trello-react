import React, { useEffect, useState } from "react";
import {
  Card,
  Heading,
  CardBody,
  CardHeader,
  Button,
  Spacer,
  Flex
} from "@chakra-ui/react";
import axios from "axios";
import FetchCard from "./FetchCard";
import { FaTrash } from "react-icons/fa";
import CreateCard from "./CreateCard";

import {apiKey,apiToken} from '../Credential'

function FetchList({ list,handleDelete }) {
  let [allCards, setAllCards] = useState([]);
  const [isFormVisible, setIsFormVisible] = useState(false);

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/lists/${list.id}/cards?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => setAllCards(response.data))
      .catch((error) => console.log(error));
    
  }, []);

  

  const addNewCard = (newCardData) => {
    setAllCards([...allCards,newCardData])
  }
  const handleCardDelete = (id) => {
    let newCards = allCards.filter(card => card.id != id)
    setAllCards(newCards)
  }
  
  const addOnClickHandler = () => {
    isFormVisible ? setIsFormVisible(false) : setIsFormVisible(true);
  };


  const archiveList = () => {
    axios
      .put(
        `https://api.trello.com/1/lists/${list.id}/closed?key=${apiKey}&token=${apiToken}&value=true`
      )
      .then((response) => handleDelete(response.data.id));
  };

  return (
    <Card minW="16rem" bg="gray" h="fit-content">
      <CardHeader>
        <Flex>
          <Heading size="sm">{list.name}</Heading>
          <Spacer />
          <FaTrash onClick={archiveList} />
        </Flex>
      </CardHeader>

      <CardBody>
        {allCards?.map((card) => {
        return(<FetchCard key={card.id} card={card} handleCardDelete = {handleCardDelete} />)
        })}
        <Button
          w="full"
          display={isFormVisible ? "none" : "block"}
          onClick={addOnClickHandler}
        >
          add a Card
        </Button>
      </CardBody>
      {isFormVisible ? <CreateCard addNewCard = {addNewCard} addOnClickHandler = {addOnClickHandler} id = {list.id}/> :'' }

    </Card>
  );
}

export default FetchList;
