import React, { useRef } from "react";
import axios from "axios";

import { apiKey, apiToken } from "../Credential";
import InputCard from "./InputCard";

function CreateCard({ addNewCard, addOnClickHandler, id }) {
  const initialRef = useRef(null);

  const submitHandler = (e) => {
    e.preventDefault();
    const cardName = initialRef.current.value;
    if (cardName != "") {
      console.log(cardName);
      axios
        .post(
          `https://api.trello.com/1/cards?name=${cardName}&idList=${id}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => addNewCard(response.data))
        .catch((error) => console.log(error));

      addOnClickHandler();
    }
    initialRef.current.value = null;
  };

  return (
    <InputCard
      initialRef={initialRef}
      submitHandler={submitHandler}
      placeholderText={"Add a card..."}
      onClickHandler={addOnClickHandler}
    />
  );
}

export default CreateCard;
