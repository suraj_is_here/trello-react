import { React, useRef } from "react";
import axios from "axios";
import {apiKey,apiToken} from '../Credential'
import InputCard from "./InputCard";

function CreateList({ addNewList, addOnClickHandler, id }) {
    const initialRef = useRef(null)

    const submitHandler = (e) => {
        e.preventDefault();
        const listName = initialRef.current.value;
        if (listName != '') {
            axios.post(`https://api.trello.com/1/lists?name=${initialRef.current.value}&idBoard=${id}&key=${apiKey}&token=${apiToken}`)
                .then((response) => addNewList(response.data))
                .catch((error) => console.log(error));
                addOnClickHandler();
        }
        
        initialRef.current.value = null;
    };

    return (
        <InputCard initialRef = {initialRef} submitHandler = {submitHandler} placeholderText ={'Enter list title'} onClickHandler = {addOnClickHandler} />
    )
    
}

export default CreateList