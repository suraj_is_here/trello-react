import React, { useRef } from "react";
import axios from "axios";
  
import {apiKey,apiToken} from '../Credential'
import InputCard from "./InputCard";
function CreateCheckList({addNewCheckList, addOnClickHandler, id }) {

    
    const initialRef = useRef(null);
    const submitHandler = (e) => {
        e.preventDefault()
        const checkListName = initialRef.current.value;
        if(checkListName != '')
            {
                axios.post(`https://api.trello.com/1/checklists?name=${checkListName}&idCard=${id}&key=${apiKey}&token=${apiToken}`)
                .then(response => addNewCheckList(response.data))
                .catch(console.error)
            }
            addOnClickHandler();
        initialRef.current.value = null;
    }
    

    return (
        <InputCard initialRef = {initialRef} submitHandler = {submitHandler} placeholderText ={'Add a checklist...'} onClickHandler = {addOnClickHandler} />
    )
}

export default CreateCheckList