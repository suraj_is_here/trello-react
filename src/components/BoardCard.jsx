import React from 'react'

import { Card, Text } from '@chakra-ui/react'

function BoardCard() {
    return (
        <Card
            h='7rem'
            w='14rem'
            alignItems='center'
            justifyItems='center'
            justifyContent='center'
            bg='gray'
        >
            <Text>My board</Text>

        </Card >
    )
}

export default BoardCard