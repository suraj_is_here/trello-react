import React, { useState, useEffect } from "react";

import axios from "axios";

import { Box, Button, Flex, Progress, Spacer } from "@chakra-ui/react";

import { IoMdCheckboxOutline } from "react-icons/io";
import Checkitems from "./Checkitems";
import { apiKey, apiToken } from "../Credential";
import CreateCheckItem from "./CreateCheckItem";

function CheckList({ checklist, deleteChecklistHandler, cardId }) {
  const [allCheckItems, setAllCheckItems] = useState([]);
  const [isFormVisible, setIsFormVisible] = useState(false);

  const archiveCheckList = (id) => {
    axios
      .delete(
        `https://api.trello.com/1/checklists/${id}?key=${apiKey}&token=${apiToken}`
      )
      .then(deleteChecklistHandler(id))
      .catch(console.error);
  };

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/checklists/${checklist.id}/checkItems?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => setAllCheckItems(response.data))
      .catch((error) => console.log(error));
  }, []);
  const deleteCheckItemHandler = (id) => {
    const newCheckItems = allCheckItems.filter(
      (checkItem) => checkItem.id !== id
    );
    setAllCheckItems(newCheckItems);
  };
  const addOnClickHandler = () => {
    isFormVisible ? setIsFormVisible(false) : setIsFormVisible(true);
  };
  const addNewCheckItem = (newCheckItem) => {
    setAllCheckItems([...allCheckItems, newCheckItem]);
  };

  const updateItemStatus = (checkItemId, state) => {
    const reqCheckItem = allCheckItems.map((checkItem) => {
      if (checkItem.id === checkItemId) {
        let copyItem = {...checkItem};
        copyItem.state = state;
        return copyItem
      }
      return checkItem;
    });
    setAllCheckItems(reqCheckItem);
  };

  const checkedItem = allCheckItems.filter(
    (checkItem) => checkItem.state === "complete"
  ).length;
  console.log(checkedItem);

  const percentage = parseInt((checkedItem / allCheckItems.length) * 100);

  return (
    <Box mt="4">
      <Flex alignItems="center">
        <span>
          <IoMdCheckboxOutline />
        </span>
        <span>{checklist.name}</span>
        <Spacer />
        <Button
          onClick={() => {
            archiveCheckList(checklist.id);
          }}
        >
          Delete
        </Button>
      </Flex>

      <Flex alignItems="center" gap="2">
        <span>{checkedItem > 0 ? percentage : 0}% </span>
        <Progress
          w="full"
          colorScheme="green"
          size="sm"
          value={checkedItem > 0 ? percentage : 0}
          rounded="md"
        />
      </Flex>

      {allCheckItems.map((checkItem) => (
        <Checkitems
          key={checkItem.id}
          checkItem={checkItem}
          deleteCheckItemHandler={deleteCheckItemHandler}
          checkListId={checklist.id}
          cardId={cardId}
          updateItemStatus={updateItemStatus}
        />
      ))}

      <Button onClick={addOnClickHandler}>Add item</Button>

      {isFormVisible ? (
        <CreateCheckItem
          key={checklist.id}
          addNewCheckItem={addNewCheckItem}
          addOnClickHandler={addOnClickHandler}
          checkListId={checklist.id}
        />
      ) : (
        ""
      )}
    </Box>
  );
}

export default CheckList;
