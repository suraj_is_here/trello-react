import { Box, Container, Button, Heading } from "@chakra-ui/react";
import { React, useState, useEffect, useRef } from "react";
import { useParams } from "react-router-dom";

import axios from "axios";
import FetchList from "./FetchList";
import CreateList from "./CreateList";

import {apiKey,apiToken} from '../Credential'



function BoardDetail() {

    const [allList, setAllList] = useState([]);
    const [isFormVisible, setIsFormVisible] = useState(false);
    const { id } = useParams();
    console.log('rendering');


    useEffect(() => {
        console.log("fetchAllList...");
        axios
            .get(
                `https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${apiToken}`
            )
            .then((response) => setAllList(response.data))
            .catch((error) => setAllList(undefined));
    }, []);

    const handleDelete= (id) => {
       let newList =  allList.filter(list => list.id != id);
       setAllList(newList)
    }

    const addNewList = (newListData) => {
        setAllList([...allList,newListData])
    }

    


    const addOnClickHandler = () => {
        isFormVisible ? setIsFormVisible(false) : setIsFormVisible(true);
    };

    
    return (

        !allList?<Heading>No Boards Found</Heading>:

        <Container
            display="flex"
            gap="4"
            mt="10"
            overflowX="auto"
            minW="full"
            h="100vh"
        >
            {allList?.map((list) => {
                return <FetchList key={list.id} list={list} handleDelete={handleDelete}  />;
            })}
            <Box minW="16rem">
                <Button
                    display={isFormVisible ? "none" : "block"}
                    id="addListButton"
                    w="full"
                    onClick={addOnClickHandler}
                >
                    Add another list
                </Button >
                

                {isFormVisible ? <CreateList addNewList = {addNewList} addOnClickHandler = {addOnClickHandler} id = {id}/> :'' }
            </Box>

        </Container>
        );
}

export default BoardDetail;
