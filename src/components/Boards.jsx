import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { Box, Grid, Text } from '@chakra-ui/react'
import CreateBoardModal from './CreateBoardModal'
import FetchBoard from './FetchBoard'
import {apiKey,apiToken} from '../Credential'
import Api from '../Api';

function Boards() {
    const [allBoards, setAllBoards] = useState([]);

    const {fetchBoard} = Api();


    useEffect(() => {
        console.log('fetch...');
        axios.get(`https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`)
            .then(response => setAllBoards(response.data))
            .catch(error => console.error)

    }, [])

    

    

    const setBoard = (board) => {
        setAllBoards([...allBoards,board])
    }
    
    const setEmptyBoard = () => 
        {
            setAllBoards([]);
        }

    // fetchBoard(setBoard(),setEmptyBoard());
    

    return (
        !allBoards ? <Heading>Loading...</Heading> : 
        <Box
            maxW='66rem'
            paddingLeft='4'
        >
            <Text
                fontWeight='bold'
                py='4'
            >Your Boards</Text>

            <Grid
                templateColumns='repeat(4, 1fr)' gap='1' rowGap='2'
            >
                <FetchBoard  allBoards={allBoards} />
                <CreateBoardModal setBoard = {setBoard} />

            </Grid>


        </Box>
    )
}

export default Boards