import React, { useRef } from "react";
import axios from "axios";

import {apiKey,apiToken} from '../Credential'
import InputCard from "./InputCard";




function CreateCheckItem({addNewCheckItem, addOnClickHandler , checkListId }) {
    const initialRef = useRef(null)

    const submitHandler = (e) => {
        e.preventDefault();
        const checkItemName = initialRef.current.value;
        if (checkItemName != '') {
            axios.post(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&key=${apiKey}&token=${apiToken}`)
                .then((response) => addNewCheckItem(response.data))
                .catch((error) => console.log(error));

                addOnClickHandler()
        }
        initialRef.current.value = null;
    };

    return (
        <InputCard initialRef = {initialRef} submitHandler = {submitHandler} placeholderText ={'Add a checkitem...'} onClickHandler = {addOnClickHandler} />
    )
}

export default CreateCheckItem



