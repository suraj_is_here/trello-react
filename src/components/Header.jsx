import { Box, Image } from '@chakra-ui/react'
import React from 'react'
import { Link} from 'react-router-dom'

function Header() {
  return (

    <Box
      w='auto'
      as='nav'
      p='4'
      borderBottom='1px'
      borderBottomColor='silver'
    >
      <Link to={'/'}>

        <Image
          src='https://trello.com/assets/87e1af770a49ce8e84e3.gif'
          filter="brightness(0) saturate(100%) invert(30%) sepia(53%) saturate(323%) hue-rotate(179deg) brightness(91%) contrast(88%)"
          alt='logo'
          h='1rem'>
        </Image>
      </Link>


    </Box >

  )
}

export default Header
