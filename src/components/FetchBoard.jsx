
import React from 'react'

import { Link } from 'react-router-dom';

import { Card, Text } from '@chakra-ui/react';



function FetchBoard({allBoards}) {
  return (
    allBoards.map((board) => {
      return (
        <Link to = {`/board/${board.id}`} >
        <Card
        key = {board.id}
          h='7rem'
          w='14rem'
          alignItems='center'
          justifyItems='center'
          justifyContent='center'
          bg='gray'

        >
          <Text>{board.name}</Text>

        </Card >
        </Link>
      )

    }

    ))


}

export default FetchBoard