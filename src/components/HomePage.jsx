import React from 'react'
import { Container } from '@chakra-ui/react'

import Boards from './Boards'
import HomePageHeader from './HomePageHeader'

function HomePage() {
    return (
        <>
            <Container
                display='flex'
                flexDirection='column'
                minW='70rem'
            >
                <HomePageHeader />
                <Boards />
            </Container>

        </>
    )
}

export default HomePage