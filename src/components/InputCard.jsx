import { React, useRef } from "react";
import { Box, Button, Input } from "@chakra-ui/react";
function InputCard({initialRef,submitHandler,placeholderText,onClickHandler}) {
    

    return (
        <form onSubmit={submitHandler}>
            <Box 
                maxW='16rem' bg='gray' h='fit-content' py='5' px='2' borderRadius='5'
            >
                <Input ref={initialRef} bg='white' placeholder={placeholderText} />
                <Button
                    mt={4}
                    mr={3}
                    colorScheme="blue"
                    type="submit"
                >
                    Add
                </Button>
                <Button mt={4}

                    onClick={onClickHandler}

                >Close</Button>
            </Box>
            </form>

    )
}

export default InputCard