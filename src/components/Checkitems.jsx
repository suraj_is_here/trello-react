import { Checkbox,Spacer,Flex } from '@chakra-ui/react'
import axios from 'axios';
import React from 'react'
import { FaTrash } from "react-icons/fa";
import {apiKey,apiToken} from '../Credential'
function Checkitems({checkItem,deleteCheckItemHandler,checkListId,cardId,updateItemStatus}) {

  const archiveCheckItem = () => {
    console.log(checkListId);
    console.log('DeleteCheckCalled');

    axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItem.id}?key=${apiKey}&token=${apiToken}`)
    .then(deleteCheckItemHandler(checkItem.id))
    .catch(console.error)
  }


  const updateCheckItem = (e) =>
    {
      let state = e.target.checked?'complete':'incomplete'
      axios.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItem.id}?state=${state}&key=${apiKey}&token=${apiToken}`)
      updateItemStatus(checkItem.id,state)
      
      
    }

  return (
    <Flex mt='2'>
      <Checkbox defaultChecked = {checkItem.state ==='complete'} onChange={updateCheckItem}  >  {checkItem.name}</Checkbox>

      <Spacer />
      <FaTrash onClick={ archiveCheckItem} />
    </Flex>
  )
}

export default Checkitems