import {
  useDisclosure,
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  ModalHeader,
  FormControl,
  FormLabel,
  Input,
  ModalFooter,
} from "@chakra-ui/react";
import axios from "axios";
import React from "react";

import { apiKey, apiToken } from "../Credential";

function CreateBoardModal({ setBoard}) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);

  const submitHandler = (e) => {
    let boardName = initialRef.current.value;
    e.preventDefault()
    console.log(boardName);
    if (boardName != "") {
      axios
        .post(
          `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => setBoard(response.data))
        .catch(console.error);
    }
    onClose();
  };

  return (
    <>
      <Button h="7rem" w="14rem" onClick={onOpen}>
        Create new board
      </Button>
      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
        <form onSubmit={submitHandler} >
          <ModalHeader>Create Board</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            
            <FormControl>
              <FormLabel>Board title</FormLabel>
              <Input ref={initialRef} placeholder="" />
            </FormControl>
            
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme="blue"
              mr={3}
              type="submit"
            >
              Save
            </Button>
            <Button onClick={onClose}>Close</Button>
          </ModalFooter>
          </form >
        </ModalContent>
      </Modal>
    </>
  );
}

export default CreateBoardModal;
