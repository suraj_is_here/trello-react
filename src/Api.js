import {apiKey,apiToken} from './Credential'
import axios from 'axios';
function Api() {
  const fetchBoard = (setBoard,setEmptyBoard) => {
    axios.get(`https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`)
            .then(response => setBoard(response.data))
            .catch(setEmptyBoard())
  }

  return {fetchBoard};

}

export default Api