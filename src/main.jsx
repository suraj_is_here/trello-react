import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { ChakraProvider } from '@chakra-ui/react'
import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements } from 'react-router-dom'
import HomePage from './components/HomePage.jsx'
import BoardDetail from './components/BoardDetail.jsx'

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path='' element={<App/>}>
      <Route index element={<HomePage/>}/>
      <Route path='/board/:id' element={<BoardDetail/>}/>
    </Route>
  )
)
ReactDOM.createRoot(document.getElementById('root')).render(
    <ChakraProvider>
      <RouterProvider router={router}/>
    </ChakraProvider>
)
